module.exports = {
    getValue(name) {
        // process.argv contains the entire command we executed inside an array
        // ['node','./src/index','--headless=0']

        // find will grab only one item from an array based on a callback function (filter)
        let match = process.argv.find(
            str =>
            // looking for '--' at index 0
            str.indexOf('--') === 0 &&
            // removes '--' from word and grabs 'headless' from 'headless=0'
            str
            .split('--')
            .join('')
            .split('=')[0] == name
        )

        // if the param was found, lets return the value '0' from 'headless='
        if (match) return match.split('=')[1]
        else return ''
    }
}