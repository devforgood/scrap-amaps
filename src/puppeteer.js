// this is the puppeteer packages
// has a bunch of useful methods to navigate for handling scraping
// we will use just a few
const puppeteer = require('puppeteer')

// we use a single browser to save resources
// (each browser takes a bunch of ram!)
var browser = null

// default options for puppeteer
var puppeteerOptions = {
    headless: process.env.HEADLESS !== '0',
    args: ['--no-sandbox', '--disable-setuid-sandbox']
}

async function getBrowserPage() {
    // we operate one page at time
    // so I decided to close the browser each time we retrieve a page
    await closeBrowser()
        // we create the browser at save it in the global var
    browser = await puppeteer.launch(puppeteerOptions)
        // we only return the page
    return await browser.newPage()
}
async function closeBrowser() {
    if (browser !== null) {
        await browser.close()
    }
}

module.exports = {
    setPuppeteerOptions(options = {}) {
        // This is ES7 sintax to assign the options keys in the same object, without replacing it
        // same as
        // Object.keys(options).forEach(key=> puppeteerOptions[key] = options[key])
        // or
        // Object.assign(puppeteerOptions, options)
        puppeteerOptions = {
            ...puppeteerOptions,
            ...options
        }
    },
    getBrowserPage,
    closeBrowser,
    async getWrapper() {
        //the wraper has different properties
        return {
            //the browser page to navigate
            page: await getBrowserPage(),

            //a wrapper for queryAll (check the original function below)
            async queryAll(selector, options = {}) {
                var { items, $ } = await queryAll(selector, {
                        page: this.page,
                        ...options
                    })
                    //map in a helper funtion to map the results providing a cheerio dom el
                var map = function(handler) {
                        //results are by default similar to jquery selectors
                        //so, we converted to array
                        //an we map using the 
                        return items.toArray().map((el, index) => handler($(el), index))
                    }
                    //this wrapper returns one extra property, map
                return {
                    items,
                    $,
                    map
                }
            },
            //helper function to retrieve a single dom element
            //based on his innerHTML content
            async findByContent(content) {
                let { items, $ } = await this.queryAll('body', {
                    waitContent: content
                })
                const cheerio = require('cheerio')
                let html = $(items[0]).html()
                if (html.indexOf(content) !== -1) {
                    let pos = html.indexOf(content)
                    for (var start = pos; start > 0; start--) {
                        if (html.charAt(start) === '<') {
                            for (var end = pos; end < html.length - 1; end++) {
                                if (html.charAt(end) === '>') {
                                    let tagHTML = html.substring(start, end + 1)

                                    const cheerio = require('cheerio')
                                    const $$ = cheerio.load(tagHTML)
                                    let body = $$($$('body').first())
                                    let el = $$($$('body > *').first())
                                    let id = require('uniqid')()
                                    el.attr('id', id)
                                    let newTagHTML = body.html()

                                    html = html.split(tagHTML).join(newTagHTML)

                                    let $ = cheerio.load(html)
                                    return {
                                        item: $(`#${id}`),
                                        $
                                    }
                                }
                            }
                        }
                    }
                } else {
                    return null
                }
            }
        }
    }
}

//a helper function to grab dom elements with cheerio
//works with dom elements that take time to charge (async website)
async function queryAll(selector, options = {}) {
    const { page } = options

    //this is a cheerio like object provided by puppeteer
    const bodyHandle = await page.$('body')
    const cheerio = require('cheerio')

    //we return a promise (this is an async function!)
    return new Promise((resolve, reject) => {
        //we save the timestamp for the the search start
        var startDate = Date.now()
        async function check() {
            //we retrieve the html from the puppeteer page 
            //important: this html represents what is loaded into the browser
            //at a certain period of time
            //this could and will change over time (second to second)
            //we can learn more about this in the follow url
            //https://developer.mozilla.org/en-US/docs/Learn/Performance/Populating_the_page:_how_browsers_work
            html = await page.evaluate(body => body.innerHTML, bodyHandle)
            
            //we use cheerio to manage the entire html
            const $ = cheerio.load(html)

            //optional param, "waitContent"
            //if we pass "HELLO WORLD" here:
            if (options.waitContent) {
                //check in the entire html for HELLO WORLD
                if ($.html().indexOf(options.waitContent) == -1) {
                    //if not found, iterate to a new search
                    return iterate()
                }
            }

            //use cheerio selector to search for the dom elements
            //similar to jquery or pure javascript
            //$(SELECTOR)  <-- jquery
            //document.querySelectorAll <-- pure js
            let items = $(selector)
            if (items.length !== 0) {
                //if items are found, let's retrieve them
                //we also return the cheerio selector $ to control the dom, we may need it
                resolve({ items, $ })
            } else {
                //if items are not found, iterate to a new search
                iterate()
            }

            function iterate() {
                //check now agains the initial timestamp
                //if we extend the timeout param (default to 10s), reject the main promise
                if (Date.now() - startDate > (options.timeout || 10000)) {
                    reject(new Error('TIMEOUT'))
                } else {
                    //if we are still in game, lets search again
                    setTimeout(() => check(), 1000)
                }
            }
        }
        check()
    })
}